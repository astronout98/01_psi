<?php
    session_start();
    include_once('config/database.php');
    $query= mysqli_query($con, "SELECT * FROM datapengajar");
    include_once('assets/header.php');
?>

        	<div class="card-group" style="padding-top: 40px; padding-bottom: 422px">
            	<div class="card">
                	<div class="card-body text-center">
                    	<table class="table table-bordered" action="request_guru.php">
                        	<thead class="table-primary font-weight-bold">
                            	<tr>
                            		<th>No</th>
                                	<th>Email Pengajar</th>
                                	<th>Nama Pengajar</th>
                                	<th>Mata Pelajaran</th>
                                	<th>Tingkat Pengajaran</th>
                                    <th>Hari</th>
                                	<th>Waktu Mulai Les</th>
                                    <th>Lama Waktu Les</th>
                                	<th></th>
                            	</tr>
                        	</thead>
                        	<?php if (mysqli_num_rows($query)==0) { ?>
		  					<tr><td colspan="6" class="t-data">Tidak ada Pengajar.</td></tr><?php }else{
				  		while ($detail = mysqli_fetch_assoc($query)) {?>
				  		<tr class="cross">
				  			<td class="t-data"><center><?php echo($detail['idPengajar']) ?></center></td>
					 		<td class="t-data"><center><?php echo($detail['emailPengajar']) ?></center></td>
					 		<td class="t-data"><center><?php echo($detail['namaLengkapPengajar']) ?></center></td>
					 		<td class="t-data"><center><?php echo($detail['mataPelajaran']) ?></b></center></td>
					 		<td class="t-data"><center><?php echo($detail['tingkatPelajaran']) ?></b></center></td>
					 		<td>
                                <select id="hari">
                                    <option value="Senin">Senin</option>
                                    <option value="Selasa">Selasa</option>
                                    <option value="Rabu">Rabu</option>
                                    <option value="Kamis">Kamis</option>
                                    <option value="Jumat">Jumat</option>
                                    <option value="Sabtu">Sabtu</option>
                                </select>                    
                            </td>
                            <td>
                                <select id="waktuMulai">
                                    <option value="08.00">08.00</option>
                                    <option value="09.00">09.00</option>
                                    <option value="10.00">10.00</option>
                                    <option value="11.00">11.00</option>
                                    <option value="12.00">12.00</option>
                                    <option value="13.00">13.00</option>
                                    <option value="14.00">14.00</option>
                                    <option value="15.00">15.00</option>
                                    <option value="16.00">16.00</option>
                                    <option value="17.00">17.00</option>
                                    <option value="18.00">18.00</option>
                                    <option value="19.00">19.00</option>
                                </select>                    
                            </td>
                            <td>
                                <select id="durasi">
                                    <option value="2">2 Jam</option>
                                    <option value="3">3 Jam</option>
                                    <option value="4">4 Jam</option>
                                    <option value="5">5 Jam</option>
                                </select>  
                            </td>

							<td class="t-data">
								<a class="btn btn-secondary btn-sm" href="#" onclick="pesan()"><i class="fa fa-eye fa-md"></i> Pesan</a>
							</td>
                            <i id="mail" hidden="hidden"><?=$detail['emailPengajar']?></i>
				  		</tr>
		  				<?php }} ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
include_once('assets/footer.php');
?>

<script type="text/javascript">
    function pesan(){
        var objHari = document.getElementById('hari');
        var hari = objHari.options[objHari.selectedIndex].value;

        var objMulai = document.getElementById('waktuMulai');
        var mulai = objMulai.options[objMulai.selectedIndex].value;        

        var objDurasi = document.getElementById('durasi');
        var durasi = objDurasi.options[objDurasi.selectedIndex].value;

        var email = document.getElementById('mail').innerText;
        
        if(hari && mulai && durasi) {
            var url = "request_guru.php?email="+email+"&hari="+hari+"&mulai="+mulai+"&durasi="+durasi;
            window.location.href = url;
        }
    }
</script>