<?php
include_once('assets/header.php');
?>
    
    <section id="contact" style="background-color: #ffff; margin-bottom: 94px">
      <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase" style="color: black">Registrasi</h2>
                <h3 class="section-subheading text-muted">Daftarkan dirimu sekarang.</h3>
            </div>
            <div class="col-lg-12 text-center">
                <div id="success"></div>
                    <button onclick ="window.location.href='register_pengajar.php'" id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Sebagai Guru</button>
                    <button onclick ="window.location.href='register_siswa.php'" id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Sebagai Siswa</button>
            </div>
        </div>
    </div>
    </section>

<?php
include_once('assets/footer.php');
?>