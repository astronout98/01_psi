<?php
include_once('assets/header.php');
?>
    
    <div class="container text-center" style="margin-top:20px; margin-bottom: 85px">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <form action="login_process.php" method="post">
                    <img class="rounded-circle w-50 mx-auto d-block" src="images/Logo.png"><br>
                    <h1 class="h3 mb-3 font-weight-normal">Masukkan email dan password anda</h1><br>
                    <input type="text" name="email" class="form-control" placeholder="Email" required autofocus>
                    <input type="password" name="password" class="form-control" style="margin-top:10px" placeholder="Password" required><br><br>
                    <input class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="Login" />
                </form>
            </div>
        </div>
    </div>
<?php
include_once('assets/footer.php');
?>