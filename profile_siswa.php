<?php
session_start();
include_once('config/database.php');
include_once('assets/header.php');
$email = $_SESSION['email'];
$cek = mysqli_query($con, "SELECT * FROM datasiswa WHERE emailSiswa = '$email'");
$profil = mysqli_fetch_assoc($cek);

?>
  <style type="text/css">
    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 300px;
        margin: auto;
        text-align: center;
        font-family: arial;
    }

    .title {
      color: grey;
      font-size: 18px;
  }

  button {
      border: none;
      outline: 0;
      display: inline-block;
      padding: 8px;
      color: white;
      background-color: #546;
      text-align: center;
      cursor: pointer;
      width: 100%;
      font-size: 18px;
  }

  a {
      text-decoration: none;
      font-size: 22px;
      color: black;
  }

  button:hover, a:hover {
      opacity: 0.7;
  }
  .card {
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
      max-width: 300px;
      margin: auto;
      text-align: center;
      font-family: arial;
  }

  .title {
      color: grey;
      font-size: 18px;
  }

  button {
      border: none;
      outline: 0;
      display: inline-block;
      padding: 8px;
      color: white;
      background-color: #546;
      text-align: center;
      cursor: pointer;
      width: 100%;
      font-size: 18px;
  }

  a {
      text-decoration: none;
      font-size: 22px;
      color: black;
  }

  button:hover, a:hover {
      opacity: 0.7;
  }
</style>

<body>

    <h2 style="text-align:center">Profilku</h2>
    <div class="card">
        <img class="rounded-circle" src="images/profil_logo.jpg" alt="user" style="width:100%">
        <h1><?php echo $profil['namaLengkapSiswa'];?></h1>
        <p class="title"></p>
        <div style="margin: 24px 0;">
            <p>Email : <?php echo $profil['emailSiswa'];?></p> 
            <p>Kelas : <?php echo $profil['kelasSiswa'];?></p>  
            <p>Alamat: <?php echo $profil['alamatSiswa'];?></p>  
            <p>No Telepon : <?php echo $profil['noTelpSiswa'];?></p>
        </div>
    </div>

</body>
<?php
include_once('assets/footer.php');
?>
</html>