<?php
    session_start();
    include_once('config/database.php');
    $no=0;
    $email = $_SESSION['email'];
    $query= mysql_query("SELECT * FROM datapembayaran where email");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" href="images/Logo.png">
        <title>Les Privat Toba Samosir</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
        <link href="css/agency.min.css" rel="stylesheet">
        <style type="text/css">
            .footer {
                height:35px;
                width: 100%;
                line-height:50px;
                background:#333;
                color:#fff;
                position: fixed;
                bottom:0px;
            }
        </style>
    </head>
    <body>
       <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <a class="navbar-brand" href="review_transaction.php">Les Privat Toba Samosir(ADMIN ZONE)</a>
            <ul class="navbar-nav ml-auto">
                <?php if ($_SESSION['role'] == 1) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="pemesanan_guru.php">Panggil Guru</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="data_pesanan.php">Pesananku</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="process_logout.php">Logout (<?= $_SESSION['namaPengguna'] ?>)</a>
                </li>
                <?php } elseif ($_SESSION['role'] == 2) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="review_request.php">Pesanan Siswa</a>
                </li>    
                <li class="nav-item active">
                    <a class="nav-link" href="process_logout.php">Logout (<?= $_SESSION['namaPengguna'] ?>)</a>
                </li>
                <?php } elseif ($_SESSION['role'] == 0) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="review_transaction.php">Transaksi</a>
                </li>    
                <li class="nav-item active">
                    <a class="nav-link" href="process_logout.php">Logout (<?= $_SESSION['namaPengguna'] ?>)</a>
                </li> 
                <?php } elseif (!isset($_SESSION['isLoggedIn'])) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="register.php">Daftar</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="login.php">Masuk</a>
                </li>
                <?php } ?>
            </ul>
        </nav>

        	<div class="card-group">
            	<div class="card">
                	<div class="card-body text-center">
                    	<table class="table table-bordered" action="request_guru.php">
                        	<thead class="table-primary font-weight-bold">
                            	<tr>
                            		<th>No</th>
                                    <th>Id Pesanan</th>
                                    <th>Email Pemesan</th>
                                	<th>Nama Pemesan</th>
                                    <th>Email Pengajar</th>
                                	<th>Nama Pengajar</th>
                                	<th>Nomor Rekening Admin</th>
                                    <th>Bukti Bayar</th>
                            	</tr>
                        	</thead>
                        	<?php if (mysql_num_rows($query)==0){?>
		  					<tr><td colspan="6" class="t-data">Tidak ada Pemesanan.</td></tr><?php }else{
				  		    while ($detail = mysql_fetch_array($query,MYSQL_ASSOC) ) {
				  		?>
				  		<tr class="cross">
				  			<td class="t-data"><center><?php echo(++$no) ?></center></td>
                            <td class="t-data"><center><?php echo($detail['idPemesanan']) ?></center></td>
					 		<td class="t-data"><center><?php echo($detail['emailPemesan']) ?></b></center></td>
					 		<td class="t-data"><center><?php echo($detail['namaPemesan']) ?></b></center></td>
                            <td class="t-data"><center><?php echo($detail['emailPengajar']) ?></b></center></td>
                            <td class="t-data"><center><?php echo($detail['namaPengajar']) ?></b></center></td>
                            <td class="t-data"><center><?php echo($detail['biaya']) ?></b></center></td>
                            <td class="t-data"><center><?php echo($detail['buktiBayar']) ?></b></center></td>
				  		</tr>
		  				<?php }} ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="footer" style="font-size: 12px; text-align: center;">
        Copyright &copy; 2019
        Designed by Kelompok 1 PSI
    </div>