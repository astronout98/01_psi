<?php
    session_start();
    include_once('config/database.php');
    include_once('assets/header.php');
?>

        <header class="masthead">
            <div class="container">
                <div class="intro-text">
                    <div class="intro-heading text-uppercase" style="font-size: 40px">Temukan pengalaman baru dalam pengembangan belajarmu dengan memanggil guru privat dengan kualitas yang telah terseleksi</div>
                 </div>
            </div>
        </header>
    <section id="services" style="background-image: url(images/bg_1.png);">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Services</h2>
            <h3 class="section-subheading text-muted">Temukan Gurumu, Ayo Belajar Sekarang Juga.</h3>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-book-reader fa-stack-1x fa-inverse"></i>
             
            </span>
            <h4 class="service-heading">Guru Les</h4>
            <p class="text-muted">Panggil Guru Les yang sudah terseleksi dengan baik</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-book-open fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Bidang Pengajaran</h4>
            <p class="text-muted">Berbagai bidang pengajaran seperti sains, seni, dan lainnya</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
             \
              <i class="far fa-credit-card fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Biaya Les</h4>
            <p class="text-muted">Biaya les yang tidak mahal dengan kualitas yang baik</p>
          </div>
        </div>
      </div>
    </section>

    <section id="about" style="background-image: url(images/bg_3.png); background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Les Privat Toba Samosir</h2>
                    <h3 class="section-subheading text-muted">Panggil Guru Privat Untuk SD, SMP dan SMA</h3>
                </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <ul class="timeline">
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>Apa itu Les Privat Toba Samosir ?</h4>
                    <h4 class="subheading"></h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Sistem Informasi yang menghubungkan antara siswa dan guru les privat di sekitaran Toba Samosir.</p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>Bagaimana dengan kualitas guru les privat ?</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Guru les privat sudah diseleksi sehingga sesuai dengan ekspektasi pemenuhan kebutuhan pemahaman tentang bidang ilmu pembelajaran.</p>
                  </div>
                </div>
              </li>
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4>Apa tujuan pendirian Sistem Informasi Les Privat Toba Samosir ?</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Memenuhi proyek akhir prodi Sistem Informasi dan menyediakan sistem informasi untuk persiapan UN dan masuk PTN.</p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <?php if (isset($_SESSION['isLoggedIn'] )) { ?>
    <section id="services" style="background-image: url(images/bg_2.png); background-size: cover;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Ayo daftar menjadi pengajar sekarang juga</h2>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-book-reader fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Klik tombol dibawah ini</h4>
            <button onclick ="window.location='register_pengajar.php'" type="button" class="btn btn-primary btn-lg">Daftar</button>
          </div>
    </section>
    <?php } ?>

<?php
  include_once('assets/footer.php');
?>