<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" href="images/Logo.png">
        <title>Les Privat Toba Samosir</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
        <link href="css/agency.min.css" rel="stylesheet">
        <style type="text/css">
            .footer {
                height:35px;
                width: 100%;
                line-height:50px;
                background:#333;
                color:#fff;
                bottom:0px;
            }
        </style>
    </head>
    <body>
       <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <a class="navbar-brand" href="index.php">Les Privat Toba Samosir</a>
            <ul class="navbar-nav ml-auto">
              <?php if (isset($_SESSION['role']) && $_SESSION['role'] == 1) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="profile_siswa.php">Profileku</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="pemesanan_guru.php">Panggil Guru</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="data_pesanan.php">Pesananku</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="process_logout.php">Logout</a>
                </li>
                <?php } elseif (isset($_SESSION['role']) && $_SESSION['role'] == 2) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="profile_guru.php">Profileku</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="review_request.php">Pesanan Siswa</a>
                </li>    
                <li class="nav-item active">
                    <a class="nav-link" href="process_logout.php">Logout</a>
                </li>
                <?php } elseif (!isset($_SESSION['isLoggedIn'])) { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="register.php">Daftar</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="login.php">Masuk</a>
                </li>
                <?php } ?>
            </ul>
        </nav>