/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.1.21-MariaDB : Database - db_les_private
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_les_private` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_les_private`;

/*Table structure for table `dataadmin` */

DROP TABLE IF EXISTS `dataadmin`;

CREATE TABLE `dataadmin` (
  `idAdmin` int(11) NOT NULL AUTO_INCREMENT,
  `emailAdmin` varchar(50) NOT NULL,
  `namaLengkapAdmin` varchar(50) NOT NULL,
  PRIMARY KEY (`idAdmin`),
  UNIQUE KEY `emailAdmin` (`emailAdmin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `dataadmin` */

/*Table structure for table `datapembayaran` */

DROP TABLE IF EXISTS `datapembayaran`;

CREATE TABLE `datapembayaran` (
  `idPembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `idPesanan` int(11) NOT NULL,
  `namaSiswa` varchar(50) NOT NULL,
  `kelasSiswa` varchar(20) NOT NULL,
  `noTelpSiswa` varchar(20) NOT NULL,
  `alamatSiswa` varchar(100) NOT NULL,
  `namaPengajar` varchar(50) NOT NULL,
  `mataPelajaran` varchar(20) NOT NULL,
  `tingkatPelajaran` varchar(20) NOT NULL,
  `noTelpPengajar` varchar(20) NOT NULL,
  `hariLes` varchar(10) NOT NULL,
  `waktuMulaiLes` varchar(10) NOT NULL,
  `lamaWaktuLes` int(11) NOT NULL,
  `totalBiaya` double NOT NULL,
  `buktiPembayaran` varchar(10) NOT NULL,
  PRIMARY KEY (`idPembayaran`),
  UNIQUE KEY `idPesanan` (`idPesanan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `datapembayaran` */

insert  into `datapembayaran`(`idPembayaran`,`idPesanan`,`namaSiswa`,`kelasSiswa`,`noTelpSiswa`,`alamatSiswa`,`namaPengajar`,`mataPelajaran`,`tingkatPelajaran`,`noTelpPengajar`,`hariLes`,`waktuMulaiLes`,`lamaWaktuLes`,`totalBiaya`,`buktiPembayaran`) values 
(1,45,'Nathan Nainggolan','4 SD','081234567890','Sitoluama','guru','Fisika','4 SD','082892738931','Senin','08.00',2,100000,'file/111.P');

/*Table structure for table `datapemesanan` */

DROP TABLE IF EXISTS `datapemesanan`;

CREATE TABLE `datapemesanan` (
  `idPesanan` int(11) NOT NULL AUTO_INCREMENT,
  `idPemesan` int(11) NOT NULL,
  `namaSiswa` varchar(50) NOT NULL,
  `kelasSiswa` varchar(50) NOT NULL,
  `noTelpSiswa` varchar(50) NOT NULL,
  `alamatSiswa` varchar(50) NOT NULL,
  `namaPengajar` varchar(20) NOT NULL,
  `mataPelajaran` varchar(20) NOT NULL,
  `tingkatPelajaran` varchar(20) NOT NULL,
  `noTelpPengajar` varchar(20) NOT NULL,
  `hariLes` varchar(10) NOT NULL,
  `waktuMulaiLes` varchar(10) NOT NULL,
  `lamaWaktuLes` int(11) NOT NULL,
  PRIMARY KEY (`idPesanan`),
  UNIQUE KEY `idPemesan` (`idPemesan`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Data for the table `datapemesanan` */

insert  into `datapemesanan`(`idPesanan`,`idPemesan`,`namaSiswa`,`kelasSiswa`,`noTelpSiswa`,`alamatSiswa`,`namaPengajar`,`mataPelajaran`,`tingkatPelajaran`,`noTelpPengajar`,`hariLes`,`waktuMulaiLes`,`lamaWaktuLes`) values 
(45,15,'Nathan Nainggolan','4 SD','081234567890','Sitoluama','guru','Fisika','4 SD','082892738931','Senin','08.00',2);

/*Table structure for table `datapengajar` */

DROP TABLE IF EXISTS `datapengajar`;

CREATE TABLE `datapengajar` (
  `idPengajar` int(11) NOT NULL AUTO_INCREMENT,
  `emailPengajar` varchar(50) NOT NULL,
  `namaLengkapPengajar` varchar(50) NOT NULL,
  `pendidikanTerakhir` varchar(20) NOT NULL,
  `pekerjaanPengajar` varchar(20) NOT NULL,
  `noTelpPengajar` varchar(20) NOT NULL,
  `jenisKelaminPengajar` varchar(10) NOT NULL,
  `mataPelajaran` varchar(20) NOT NULL,
  `tingkatPelajaran` varchar(20) NOT NULL,
  `alamatPengajar` varchar(100) NOT NULL,
  `noRekening` int(11) NOT NULL,
  `namaRekening` varchar(50) NOT NULL,
  `fotoPengajar` varchar(20) NOT NULL,
  PRIMARY KEY (`idPengajar`),
  UNIQUE KEY `emailPengajar` (`emailPengajar`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `datapengajar` */

insert  into `datapengajar`(`idPengajar`,`emailPengajar`,`namaLengkapPengajar`,`pendidikanTerakhir`,`pekerjaanPengajar`,`noTelpPengajar`,`jenisKelaminPengajar`,`mataPelajaran`,`tingkatPelajaran`,`alamatPengajar`,`noRekening`,`namaRekening`,`fotoPengajar`) values 
(7,'guru@gmail.com','guru','Kuliah','Guru','082892738931','Pria','Fisika','4 SD','Sitoluama',2147483647,'Guru','');

/*Table structure for table `datapengguna` */

DROP TABLE IF EXISTS `datapengguna`;

CREATE TABLE `datapengguna` (
  `idPengguna` int(11) NOT NULL AUTO_INCREMENT,
  `emailPengguna` varchar(50) NOT NULL,
  `kataSandiPengguna` varchar(50) NOT NULL,
  `rolePengguna` int(11) NOT NULL,
  PRIMARY KEY (`idPengguna`),
  UNIQUE KEY `emailPengguna` (`emailPengguna`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `datapengguna` */

insert  into `datapengguna`(`idPengguna`,`emailPengguna`,`kataSandiPengguna`,`rolePengguna`) values 
(37,'nathan.nainggolan15@gmail.com','1404834e52a4c6cac9444f1fb3c62d3c',1),
(38,'guru@gmail.com','77e69c137812518e359196bb2f5e9bb9',2);

/*Table structure for table `datasiswa` */

DROP TABLE IF EXISTS `datasiswa`;

CREATE TABLE `datasiswa` (
  `idSiswa` int(11) NOT NULL AUTO_INCREMENT,
  `emailSiswa` varchar(50) NOT NULL,
  `namaLengkapSiswa` varchar(50) NOT NULL,
  `kelasSiswa` varchar(50) NOT NULL,
  `noTelpSiswa` varchar(20) NOT NULL,
  `alamatSiswa` varchar(100) NOT NULL,
  `noTelpOrangTua` int(20) NOT NULL,
  PRIMARY KEY (`idSiswa`),
  UNIQUE KEY `emailSiswa` (`emailSiswa`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `datasiswa` */

insert  into `datasiswa`(`idSiswa`,`emailSiswa`,`namaLengkapSiswa`,`kelasSiswa`,`noTelpSiswa`,`alamatSiswa`,`noTelpOrangTua`) values 
(15,'nathan.nainggolan15@gmail.com','Nathan Nainggolan','4 SD','081234567890','Sitoluama',2147483647);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
