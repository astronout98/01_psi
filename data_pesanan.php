<?php
session_start();
include_once('config/database.php');
include_once('assets/header.php');
$no=0;
$email = $_SESSION['email'];
$cek = mysqli_query($con, "SELECT * FROM datasiswa WHERE emailSiswa = '$email'");
$siswa = mysqli_fetch_assoc($cek);
$id = $siswa['idSiswa'];
$query= mysqli_query($con, "SELECT * FROM datapemesanan where idPemesan = '$id'");
$pesanan = mysqli_fetch_assoc($query);
$idPesanan = $pesanan['idPesanan'];
$pembayaran = mysqli_query($con, "SELECT * FROM datapembayaran where idPesanan = '$idPesanan'");
?>

<p style="font-style: italic; color: red">*Harap mengupload bukti pembayaran sebelum mensubmit</p>
<div class="card-group" style="padding-bottom: 422px">
 <div class="card">
   <div class="card-body text-center">
     <table class="table table-bordered" action="request_guru.php">
       <thead class="table-primary font-weight-bold">
         <tr>
          <th>No</th>
          <th>Nama Siswa</th>
          <th>Nama Pengajar</th>
          <th>Mata Pelajaran</th>
          <th>Tingkat Pengajaran</th>
          <th>Jam Mengajar</th>
          <th>Biaya</th>
          <th>Nomor Rekening Admin</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <?php if (mysqli_num_rows($query)==0){?>
       <tr><td colspan="6" class="t-data">Tidak ada Pemesanan.</td></tr>
     <?php } else {
      foreach ($query as $detail) {
        ?>
        <tr class="cross">
         <td class="t-data"><center><?php echo(++$no) ?></center></td>
         <td class="t-data"><center><?php echo $siswa['namaLengkapSiswa'] ?></center></td>
         <td class="t-data"><center><?php echo($detail['namaPengajar']) ?></center></td>
         <td class="t-data"><center><?php echo($detail['mataPelajaran']) ?></b></center></td>
         <td class="t-data"><center><?php echo($detail['tingkatPelajaran']) ?></b></center></td>
         <td class="t-data"><center><?= $detail['hariLes'].' / '.$detail['waktuMulaiLes'].' WIB ('.$detail['lamaWaktuLes'].' Jam)' ?></b></center></td>
         <td class="t-data"><center><?php echo('BNI(1234567890)') ?></b></center></td>
         <?php 
         $dtl = mysqli_fetch_assoc($pembayaran);
         if ($dtl['buktiPembayaran'] == '') { ?>
          <td class="t-data"><center><?php echo($dtl['totalBiaya']) ?></b></center></td>
          <td class="t-data">
            <form action="aksi.php" method="post" enctype="multipart/form-data">
              <label class="btn btn-info btn-file">
                Upload <input type="file" style="display: none;" name="file" required="">
              </label>
              <label class="btn btn-success">
                Submit<input type="submit" style="display: none;" name="upload">
              </label>
            </form>
          </td>
        <?php } else { ?>
          <td class="t-data">Anda Sudah Upload Bukti Bayar</td>
          <td class="t-data">-</td>
        <?php } ?>
      </tr>
    <?php } } ?>
  </table>
</div>
</div>
</div>
</div>

<?php
include_once('assets/footer.php');
?>