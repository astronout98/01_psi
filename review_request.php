<?php
    session_start();
    include_once('config/database.php');
    $no=0;
    $email = $_SESSION['email'];
    $cek = mysqli_query($con, "SELECT * FROM datapengajar WHERE emailPengajar = '$email'");
    $guru = mysqli_fetch_assoc($cek);
    $namaPengajar = $guru['namaLengkapPengajar'];
    $query= mysqli_query($con, "SELECT * FROM datapemesanan where namaPengajar = '$namaPengajar'");
    $pesanan = mysqli_fetch_assoc($query);
    $idPesanan = $pesanan['idPesanan'];
    $querypembayaran = mysqli_query($con, "SELECT * FROM datapembayaran where idPesanan = '$idPesanan'");
    include_once('assets/header.php');
?>

        	<div class="card-group" style="padding-top: 40px; padding-bottom: 422px">
            	<div class="card">
                	<div class="card-body text-center">
                    	<table class="table table-bordered" action="process_pembayaran.php">
                        	<thead class="table-primary font-weight-bold">
                            	<tr>
                            		<th>No</th>
                                    <th>Nama Siswa</th>
                                	<th>Kelas</th>
                                	<th>No Telepon Siswa</th>
                                	<th>Alamat Siswa</th>
                                	<th>Jam Mengajar</th>
                                    <th>Status Pembayaran</th>
                            	</tr>
                        	</thead>
                        	<?php if (mysqli_num_rows($query)==0){?>
		  					<tr><td colspan="6" class="t-data">Tidak ada Pemesanan.</td></tr><?php }else{
				  		    foreach ($query as $detail) {
				  		?>
				  		<tr class="cross">
				  			<td class="t-data"><center><?php echo(++$no) ?></center></td>
                            <td class="t-data"><center><?php echo($detail['namaSiswa']) ?></center></td>
					 		<td class="t-data"><center><?php echo($detail['kelasSiswa']) ?></b></center></td>
					 		<td class="t-data"><center><?php echo($detail['noTelpSiswa']) ?></b></center></td>
                            <td class="t-data"><center><?php echo($detail['alamatSiswa']) ?></b></center></td>
					 		<td class="t-data"><center><?= $detail['hariLes'].' / '.$detail['waktuMulaiLes'].' WIB ('.$detail['lamaWaktuLes'].' Jam)' ?></b></center></td>
                            <?php {
                                $dtl = mysqli_fetch_assoc($querypembayaran);
                                if ($dtl['buktiPembayaran'] != '') { ?>
                                <td class="t-data">Sudah dibayar, Silahkan Cek Rekening anda</td>
                            <?php } else{ ?>
                                <td class="t-data">Menunggu Pembayaran</td>
                            <?php } ?>
				  		</tr>
		  				<?php }}} ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
include_once('assets/footer.php');
?>

    