<?php
include_once('assets/header.php');
?>
    
      <div class="container" style="margin-top:20px">
        <center><h1>Daftarkan diri anda sebagai Pengajar</h1></center>
        <br>
        <div class="row">
            <div class="col-md-6">
                <form action="register_process.php?role=2" method="POST">
                  <div class="form-group">
                    <p>Nama Lengkap*</p>
                    <input class="form-control" name="nama" type="nama" required>
                  </div>
                  <div class="form-group">
                    <p>Email*</p>
                    <input class="form-control" name="email" type="email" required>
                  </div>
                  <div class="form-group">
                    <p>Nomor Telepon*</p>
                    <input class="form-control" name="phone" type="phone" required>
                  </div>
                  <div class="form-group">
                    <p>Jenis Kelamin*</p>
                    <input class="form-control" name="gender" type="gender" placeholder="Pria/Wanita" required>
                  </div>
                   <div class="form-group">
                    <p>Pendidikan Terakhir*</p>
                    <input class="form-control" name="pendidikan" type="pendidikan" placeholder="Contoh: SMA" required>
                  </div>
                  <div class="form-group">
                    <p>Pekerjaan*</p>
                    <input class="form-control" name="pekerjaan" type="pekerjaan" required>
                  </div>
                  <div class="form-group">
                    <p>Mata Pelajaran*</p>
                    <input class="form-control" name="matapelajaran" type="matapelajaran" placeholder="Contoh : Fisika" required>
                  </div>
                  <div class="form-group">
                    <p>Tingkat Pelajaran</p>
                    <input class="form-control" name="kelas" type="kelas" placeholder="Contoh : 4 SD" required>
                  </div>
                   
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                    <p>Alamat*</p>
                    <input class="form-control" name="alamat" type="alamat" required>
                  </div>
                  <div class="form-group">
                    <p>Nomor Rekening*</p>
                    <input class="form-control" name="norekening" type="norekening"required>
                  </div>
                  <div class="form-group">
                    <p>Nama sesuai buku rekening*</p>
                    <input class="form-control" name="namarek" type="namarek" required>
                  </div>
                   <div class="form-group">
                    <p>Password*</p>
                    <input class="form-control" name="pwd" type="password" required>
                  </div>
                  <div class="form-group">
                    <p>Masukkan kembali password*</p>
                    <input class="form-control" name="pwds" type="password" required>
                  </div>
                  <div class="col-md-4 offset-md-4">
                      <input class="btn btn-lg btn-primary btn-block" type="submit" name="register_submit" value="Register" />
                  </div>
                    </div>
          </form>

                </div>
        </div>
      </div>
<?php
include_once('assets/footer.php');
?>