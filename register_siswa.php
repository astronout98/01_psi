<?php
include_once('assets/header.php');
?>
    
    <div class="container" style="margin-top:20px">
        <center><h1>Daftarkan diri anda sebagai Siswa</h1></center>
        <br>
        <div class="row">
            <div class="col-md-12">
                <form action="register_process.php?role=1" method="POST">
                     <div class="form-group">
                    <p>Nama Lengkap*</p>
                    <input class="form-control" name="nama" type="nama" required>
                  </div>
                  <div class="form-group">
                    <p>Email*</p>
                    <input class="form-control" name="email" type="email" required>
                  </div>
                  <div class="form-group">
                    <p>Kelas*</p>
                    <input class="form-control" name="kelas" type="kelas" placeholder="4 SD" required>
                  </div>
                  <div class="form-group">
                    <p>Nomor Telepon*</p>
                    <input class="form-control" name="phone" type="phone" placeholder="" required>
                  </div>
                   <div class="form-group">
                    <p>Alamat*</p>
                    <input class="form-control" name="alamat" type="alamat" required>
                  </div>
                  <div class="form-group">
                    <p>Nomor Telepon Orang Tua*</p>
                    <input class="form-control" name="noTelp" type="noTelp" required>
                  </div>
                  <div class="form-group">
                    <p>Password*</p>
                    <input class="form-control" name="pwd" type="password" required>
                  </div>
                  <div class="form-group">
                    <p>Masukkan kembali password*</p>
                    <input class="form-control" name="pwds" type="password" required>
                  </div>
                </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 offset-md-5">
                            <input class="btn btn-lg btn-primary btn-block" type="submit" value="Register" />
                        </div>
                    </div>
                    <br><br><br>
                </form>
            </div>
        </div>
    </div>
<?php
include_once('assets/footer.php');
?>